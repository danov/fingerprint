package org.fpr;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.fpr.obj.Fingerprint;
import org.fpr.obj.Minutia;
import org.fpr.obj.Template;
import org.opencv.android.Utils;
import org.opencv.core.Point;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class FPR extends Activity {

	private static final String TAG = "FPR::Activity";

	private static final String ext = ".png";
	private static final String file1 = "1_1";
	private static final String filename1 = file1 + ext;
	private static String file2 = "2_2";
	private static String filename2 = file2 + ext;
	private static final String path = "mnt/sdcard/fpr/";

	private static final String prints = path + "prints/";
	private static final String preproc = path + "preproc/";
	private static final String threshold = path + "threshold/";
	private static final String dirpix = path + "dirpix/";
	private static final String dirblock = path + "dirblock/";
	private static final String skeleton = path + "skeleton/";
	private static final String minutia = path + "minutia/";
	private static final String match = path + "match/";

	private TouchImageView imageView;
	private Bitmap bmp;

	private MenuItem mItemLoad;
	private MenuItem mItemMinutia;
	private MenuItem mItemThreshold;
	private MenuItem mItemSkeletonize;
	private MenuItem mItemDirectionalPixel;
	private MenuItem mItemDirectionalBlock;

	private MenuItem mItemBatch;

	public FPR() {
		Log.i(TAG, "Instantiated new " + this.getClass());
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);

		imageView = (TouchImageView) findViewById(R.id.imageView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i(TAG, "onCreateOptionsMenu");
		mItemLoad = menu.add("Load image & Preprocess");
		mItemDirectionalPixel = menu.add("Directional pixel-wise");
		mItemDirectionalBlock = menu.add("Directional block-wise");
		mItemThreshold = menu.add("Adaptive Threshold");
		mItemSkeletonize = menu.add("Skeletonize");
		mItemMinutia = menu.add("Minutia detection");
		mItemBatch = menu.add("Batch compare");
		return true;
	}

	private Fingerprint fp;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.i(TAG, "Menu Item selected " + item);
		if (item == mItemLoad) {
			bmp = BitmapFactory.decodeFile(prints + filename1);
			fp = new Fingerprint(bmp);
			Utils.matToBitmap(fp.getPreprocessed(), bmp);
			saveBitmap(bmp, preproc + filename1);
		} else if (item == mItemDirectionalPixel) {
			Utils.matToBitmap(fp.getDirPixelwise(), bmp);
			saveBitmap(bmp, dirpix + filename1);
		} else if (item == mItemDirectionalBlock) {
			Utils.matToBitmap(fp.drawSingularity(fp.getDirBlockwise()), bmp);
			saveBitmap(bmp, dirblock + filename1);
		} else if (item == mItemThreshold) {
			Utils.matToBitmap(fp.getThresholded(), bmp);
			saveBitmap(bmp, threshold + filename1);
		} else if (item == mItemSkeletonize) {
			Utils.matToBitmap(fp.getSkeleton(), bmp);
			saveBitmap(bmp, skeleton + filename1);
		} else if (item == mItemMinutia) {
			Utils.matToBitmap(fp.drawMinutia(fp.getSkeleton()), bmp);
			saveBitmap(bmp, minutia + filename1);
		} else if (item == mItemBatch) {
			Utils.matToBitmap(fp.drawMinutia(fp.getSkeleton()), bmp);
			comparision();
		}

		if (bmp != null) {
			imageView.setImageBitmap(bmp);
			imageView.setMaxZoom(10.0f);
		}

		return true;
	}

	private void comparision() {
		Template tp = fp.getTemplate();
		Log.i(TAG, "Minutiae nbr: " + tp.numMinutiae());
		Fingerprint fp2;
		int i = 1, j = 2;
		for(i = 1; i < 11; i++) {
			for (j = 1; j < 9; j++) {
				file2 = i + "_" + j;
				filename2 = file2 + ext;
				Bitmap bmp2 = BitmapFactory.decodeFile(prints + filename2);
				fp2 = new Fingerprint(bmp2);

				if (i == 1) switch (j) {
					case 2:
						fp2.getTemplate().setCenter(new Point(259, 239));
						break;
					case 3:
						fp2.getTemplate().setCenter(new Point(360, 170));
						break;
					case 4:
						fp2.getTemplate().setCenter(new Point(215, 35));
						break;
					case 5:
						fp2.getTemplate().setCenter(new Point(295, 360));
						break;
					case 6:
						fp2.getTemplate().setCenter(new Point(210, 250));
						break;
					case 7:
						fp2.getTemplate().setCenter(new Point(270, 250));
						break;
					case 8:
						fp2.getTemplate().setCenter(new Point(265, 220));
						break;
				}

				Utils.matToBitmap(fp2.getPreprocessed(), bmp2);
				saveBitmap(bmp2, preproc + filename2);

				Utils.matToBitmap(fp2.getDirPixelwise(), bmp2);
				saveBitmap(bmp2, dirpix + filename2);

				Utils.matToBitmap(fp2.drawSingularity(fp2.getDirBlockwise()), bmp2);
				saveBitmap(bmp2, dirblock + filename2);

				Utils.matToBitmap(fp2.getThresholded(), bmp2);
				saveBitmap(bmp2, threshold + filename2);

				Utils.matToBitmap(fp2.getSkeleton(), bmp2);
				saveBitmap(bmp2, skeleton + filename2);

				Utils.matToBitmap(fp2.drawMinutia(fp2.getSkeleton()), bmp2);
				saveBitmap(bmp2, minutia + filename2);

				LinkedHashMap<Minutia, Minutia> matches = tp.getMatches(fp2.getTemplate());
				ArrayList<Double> scores = new ArrayList<Double>();
				ArrayList<Double> azimuths = new ArrayList<Double>();
				double score = tp.calculateScore(matches, scores, azimuths);

				Utils.matToBitmap(tp.drawReference(fp.getSkeleton(), matches), bmp2);
				saveBitmap(bmp2, match + file1 + "x" + file2 + ext);

				Utils.matToBitmap(fp2.getTemplate().drawProbe(fp2.getSkeleton(), matches), bmp2);
				saveBitmap(bmp2, match + file2 + "x" + file1 + ext);

				String csvhead = filename2 + ", " + Double.toString(score) + ", " + fp2.getTemplate().numMinutiae();
				String csv = csvhead + ", " + scores.toString().substring(1, scores.toString().length() - 1);
				csv += System.getProperty("line.separator") + csvhead + ", " + azimuths.toString().substring(1, azimuths.toString().length() - 1);
				saveCSV(csv, match + file2 + ".csv");
				Log.i(TAG, csv);
			}
		}
	}

	public void saveCSV(String csv, String file) {
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(file));
			out.write(csv);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveBitmap(Bitmap bmp, String file) {
		try {
			FileOutputStream out = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
