package org.fpr.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

import org.fpr.obj.Minutia;
import org.fpr.obj.MinutiaType;
import org.opencv.core.Mat;

public class GrayMatWrapper {

	private Mat img;
	private byte[] pixels;
	private int w;
	private int h;
	private int blockw;
	private int blockh;
	private int chans; // ???
	private int blockSize;

	public static final int grayLevelPerUnit = 60;

	public GrayMatWrapper(Mat src) {
		img = src;
		w = src.width();
		h = src.height();
		chans = src.channels();
		pixels = new byte[(int) (src.total() * src.channels())];
		src.get(0, 0, pixels);
		setBlockSize(16);
	}

	public int getWidth() {
		return w;
	}

	public int getHeight() {
		return h;
	}

	public int getChans() {
		return chans;
	}

	public void setPixel(int x, int y, int val) {
		pixels[y * w * chans + x * chans] = (byte) val;
	}

	public int getPixel(int x, int y) {
		return (int) pixels[y * w * chans + x * chans] & 0xff;
	}

	public void setBlockSize(int size) {
		blockSize = size;
		blockh = h / blockSize;
		blockw = w / blockSize;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public int getBlockWidth() {
		return blockw;
	}

	public int getBlockHeight() {
		return blockh;
	}

	public void setBlock(int blockx, int blocky, int val) {
		int x = blockx * blockSize;
		int y = blocky * blockSize;
		for (int p = 0; p < blockSize && x + p < w; p++) {
			for (int q = 0; q < blockSize && y + q < h; q++) {
				setPixel(x + p, y + q, val);
			}
		}
	}

	public int getMajorityVote(int blockx, int blocky) {
		int[] votes = new int[] { 0, 0, 0, 0 };
		int zeros = 0;
		int color = 0;
		int x = blockx * blockSize;
		int y = blocky * blockSize;

		if (blockx > blockw || blocky > blockh) return 0;

		for (int p = 0; p < blockSize && x + p < w; p++) {
			for (int q = 0; q < blockSize && y + q < h; q++) {
				int val = getPixel(x + p, y + q);
				if (val == 0) {
					zeros++;
				} else {
					votes[Math.round(val / (float) grayLevelPerUnit) - 1]++;
				}
			}
		}

		int max = 0;
		for (int p = 0; p < 4; p++) {
			zeros -= votes[p];
			if (votes[max] < votes[p]) max = p;
		}

		if (zeros > 2 * GrayMatWrapper.grayLevelPerUnit) color = 0;
		else color = GrayMatWrapper.grayLevelPerUnit * (max + 1);

		return color;
	}

	public int getBlock(int blockx, int blocky) {
		return getPixel(blockx * blockSize, blocky * blockSize);
	}

	public int getPoincareIndex(int blockx, int blocky) {
		int angle = 0;
		int ksize = 3;
		int kborderlen = (ksize - 1) * 4;
		for (int i = 0; i < kborderlen; i++) {
			int[] pos = getKernelPosByIndex(i, blockx, blocky, ksize);
			int val = getBlock(pos[0], pos[1]) / grayLevelPerUnit - 1;

			pos = getKernelPosByIndex(i + 1, blockx, blocky, ksize);
			int next = getBlock(pos[0], pos[1]) / grayLevelPerUnit - 1;
			int diff = val - next;
			if (Math.abs(diff) > 2) {
				if (diff > 0) diff -= 4;
				else diff += 4;
			}
			angle += diff;
		}

		return angle;
	}

	public int[] getKernelPosByIndex(int i, int x, int y, int ksize) {
		int[] pos = new int[] { x, y };
		int ksz = ksize - 1;
		int distance = ksz / 2;
		int rule = (i / ksz) % 4;
		switch (rule) {
			case 0:
				pos[0] = x - distance + i % ksz;
				pos[1] = y - distance;
				break;
			case 1:
				pos[0] = x + distance;
				pos[1] = y - distance + i % ksz;
				break;
			case 2:
				pos[0] = x + distance - i % ksz;
				pos[1] = y + distance;
				break;
			case 3:
				pos[0] = x - distance;
				pos[1] = y + distance - i % ksz;
				break;
		}

		return pos;
	}

	public int crossingNumber(int x, int y) {
		int whites = 0;
		int count = 0;
		int ksize = 3;
		int kborderlen = (ksize - 1) * 4;
		for (int i = 0; i < kborderlen; i++) {
			int pos[] = getKernelPosByIndex(i, x, y, ksize);
			int one = getPixel(pos[0], pos[1]);
			if(one != 0) whites++;
			
			pos = getKernelPosByIndex(i + 1, x, y, ksize);
			int two = getPixel(pos[0], pos[1]);
			if (one != two) {
				count++;
			}
		}

		count /= 2;
		return count == 1 && 2 * whites > kborderlen ? 0 : count;
	}

	public void saveToMat() {
		img.put(0, 0, pixels);
	}

	public double findOrientation(int x, int y, MinutiaType type) {
		int ksize = 3;
		int range = 16;
		int kborderlen = (ksize - 1) * 4;
		int node[] = new int[] { x, y };

		LinkedList<int[]> frontier = new LinkedList<int[]>();
		HashSet<Integer> visited = new HashSet<Integer>();
		frontier.add(node);
		visited.add(y * w + x);
		ArrayList<Double> angles = new ArrayList<Double>();
		while (!frontier.isEmpty()) {
			node = frontier.poll();
			double distance = Math.sqrt(Math.pow(node[0] - x, 2) + Math.pow(y - node[1], 2));
			if (distance < range && node[0] > 1 && node[1] > 1 && node[0] < w - 2 && node[1] < h - 2) {
				for (int j = 0; j < kborderlen; j++) {
					int[] pos = getKernelPosByIndex(j, node[0], node[1], ksize);
					if (!visited.contains(pos[1] * w + pos[0])) {
						int onePixel = getPixel(pos[0], pos[1]);
						if (onePixel != 0) {
							int count = crossingNumber(pos[0], pos[1]);
							if (count == 2) {
								visited.add(pos[1] * w + pos[0]);
								frontier.add(pos);
							} else {
								// in case of too close minutae
								return Math.atan2(y - pos[1], pos[0] - x);
							}
						}
					}
				}
			} else {
				// it is endpoint, if it is not in range or just hit the boundry of the screen
				angles.add(Math.atan2(y - node[1], node[0] - x));
			}
		}

		int best = 0;
		double bdiff = Double.MAX_VALUE;
		for (int i = 0; i < angles.size(); i++) {
			int diff = 0;
			for (int j = 0; j < angles.size(); j++) {
				if (i != j) {
					diff += Minutia.getAbsAngleDiff(angles.get(i), angles.get(j));
				}
			}

			if (diff < bdiff) {
				best = i;
				bdiff = diff;
			}
		}

		return angles.get(best);
	}
}
