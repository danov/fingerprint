package org.fpr.utils;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class CvHelper {

	private static Mat[] structuring = null;
	private static final byte[][] structarrs = new byte[][] { new byte[] { 1, 1, 1, 0, 1, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0, 0, 1, 1, 1 },
			new byte[] { 1, 1, 0, 1, 1, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0, 1, 0, 1, 1 } };

	private static final double[][] kernels = new double[][] { 
			new double[] {
							0, 0, 0, 0, 0, 
							0, 0, 0, 0, 0, 
							-1, -1, 4, -1, -1, 
							0, 0, 0, 0, 0, 
							0, 0, 0, 0, 0, 
						}, // horizontal (0)
			new double[] { 0, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, 4, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, 0, }, // anti-diagonal (pi/4)
			new double[] { 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 4, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, }, // vertical (pi/2)
			new double[] { -1, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, -1, }, // main diagonal (3/4 pi)
	};

	public static void directionalPixelwise(Mat src, Mat dst) {
		Mat[] med = new Mat[4];
		byte[][] all = new byte[4][];
		byte[] img = new byte[(int) (src.total() * src.channels())];

		if (src != dst) src.copyTo(dst);
		for (int i = 0; i < 4; i++) {
			med[i] = new Mat();
			Mat kernel = new Mat(5, 5, CvType.CV_32FC1);
			kernel.put(0, 0, kernels[i]);
			Imgproc.filter2D(src, med[i], -1, kernel);
			all[i] = new byte[(int) (med[i].total() * med[i].channels())];
			med[i].get(0, 0, all[i]);
		}

		for (int j = 0; j < all[0].length; j++) {
			int min = 0;
			for (int k = 0; k < 4; k++) {
				int val = ((int) all[k][j] & 0xff);
				if (val < ((int) all[min][j] & 0xff)) {
					min = k;
				} else if (val == 0 && k != 0) {
					min = -1;
					break;
				}
			}
			img[j] = (byte) ((min + 1) * GrayMatWrapper.grayLevelPerUnit);
		}

		dst.put(0, 0, img);
	}

	public static void directionalBlockwise(Mat src, Mat dst) {
		if (src != dst) src.copyTo(dst);
		GrayMatWrapper grm = new GrayMatWrapper(dst);

		// +1 needed to black-out the last pixels, belonging to no block
		for (int y = 0; y < grm.getBlockHeight() + 1; y++) {
			for (int x = 0; x < grm.getBlockWidth() + 1; x++) {
				int majority = grm.getMajorityVote(x, y);
				grm.setBlock(x, y, majority);
			}
		}

		grm.saveToMat();
	}

	public static void inverseGray(Mat src) {
		Mat temp = new Mat(src.size(), src.type());
		temp.setTo(new Scalar(255, 255, 255));
		Core.subtract(temp, src, src);
		temp.release();
	}

	public static void skeletonize(Mat src, Mat dst) {
		Mat temp = new Mat();
		Mat old = new Mat();
		src.copyTo(dst);
		src.copyTo(old);
		generateStructuringElements();
		while (true) {
			for (int i = 0; i < structuring.length; i += 2) {
				if ((i / 2) % 2 == 0) {
					thinning(dst, structuring[i], structuring[i + 1], temp);
				} else {
					thinning(temp, structuring[i], structuring[i + 1], dst);
				}
			}

			if (Core.countNonZero(old) == Core.countNonZero(dst)) {
				break;
			}

			dst.copyTo(old);
		};
		old.release();
		temp.release();
	}

	public static void thinning(Mat a, Mat c, Mat d, Mat dst) {
		hitOrMiss(a, c, d, dst);
		Core.bitwise_not(dst, dst);
		Core.bitwise_and(a, dst, dst);
	}

	public static void hitOrMiss(Mat a, Mat c, Mat d, Mat dst) {
		Mat temp = new Mat();
		Core.bitwise_not(a, temp);
		Imgproc.erode(a, dst, c);
		Imgproc.erode(temp, temp, d);
		Core.bitwise_and(dst, temp, dst);
		temp.release();
	}

	public static void generateStructuringElements() {
		if (structuring != null) return;
		structuring = new Mat[structarrs.length * 4];

		for (int i = 0; i < structarrs.length; i++) {
			structuring[i] = new Mat(3, 3, CvType.CV_8UC1);
			structuring[i].put(0, 0, structarrs[i]);
			for (int j = 0; j < 3; j++) {
				// Rotate clockwise
				structuring[(j + 1) * structarrs.length + i] = new Mat(3, 3, CvType.CV_8UC1);
				Core.flip(structuring[j * structarrs.length + i].t(), structuring[(j + 1) * structarrs.length + i], 0);
			}
		}
	}
}
