package org.fpr.obj;

import org.fpr.utils.CvHelper;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.graphics.Bitmap;

public class Fingerprint {

	private Mat original;
	private Mat preprocessed;
	private Mat dirPixelwise;
	private Mat dirBlockwise;
	private Mat mask;
	private Mat thresholded;
	private Mat skeleton;
	private Template template = new Template();

	public Fingerprint(Bitmap bmp, Bitmap bmp2) {
		dirBlockwise = new Mat();
		skeleton = new Mat();
		Utils.bitmapToMat(bmp, dirBlockwise);
		Imgproc.cvtColor(dirBlockwise, dirBlockwise, Imgproc.COLOR_BGR2GRAY);
		Utils.bitmapToMat(bmp2, skeleton);
		Imgproc.cvtColor(skeleton, skeleton, Imgproc.COLOR_BGR2GRAY);
	}
	
	public Fingerprint(Bitmap bmp) {
		original = new Mat();
		preprocessed = new Mat();
		Utils.bitmapToMat(bmp, original);
		Imgproc.cvtColor(original, preprocessed, Imgproc.COLOR_BGR2GRAY);
		Imgproc.equalizeHist(preprocessed, preprocessed);
		// With gaussian blur will work better, but can't find out why it introduces noise on black backgrounds
		Imgproc.medianBlur(preprocessed, preprocessed, 5);
		CvHelper.inverseGray(preprocessed);
		Imgproc.equalizeHist(preprocessed, preprocessed);
		cropImage();
	}

	private void cropImage() {

	}

	public Mat getOriginal() {
		return original;
	}

	public Mat getPreprocessed() {
		return preprocessed;
	}

	public Mat getDirPixelwise() {
		if (dirPixelwise == null) {
			dirPixelwise = new Mat();
			CvHelper.directionalPixelwise(preprocessed, dirPixelwise);
			// With gaussian blur will work better, but can't find out why it introduces noise on black backgrounds
			Imgproc.medianBlur(dirPixelwise, dirPixelwise, 3);
		}

		return dirPixelwise;
	}

	public Mat getDirBlockwise() {
		if (dirBlockwise == null) {
			dirBlockwise = new Mat();
			CvHelper.directionalBlockwise(getDirPixelwise(), dirBlockwise);
		}

		return dirBlockwise;
	}

	public Mat getMask() {
		if (mask == null) {
			mask = new Mat();
			Imgproc.threshold(getDirBlockwise(), mask, 10, 255, Imgproc.THRESH_BINARY);
			Imgproc.erode(mask, mask, Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, new Size(3, 3)), new Point(-1, -1), 3);
		}

		return mask;
	}

	public Mat getThresholded() {
		if (thresholded == null) {
			thresholded = new Mat();
			Imgproc.adaptiveThreshold(preprocessed, thresholded, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 17, 0);
		}

		return thresholded;
	}

	public Mat getSkeleton() {
		if (skeleton == null) {
			Mat temp = new Mat();
			skeleton = new Mat();
			Core.bitwise_and(getThresholded(), getMask(), temp);
			CvHelper.skeletonize(temp, skeleton);
		}

		return skeleton;
	}

	public Mat drawSingularity(Mat src) {
		if (!template.singularitiesCalculated()) {
			template.extractSingularities(getDirBlockwise());
		}

		Mat temp = new Mat();
		if (src.channels() == 1) Imgproc.cvtColor(src, temp, Imgproc.COLOR_GRAY2BGR);
		else src.copyTo(temp);
		template.drawSingularities(temp);
		return temp;
	}

	public Mat drawMinutia(Mat src) {
		if (!template.minutiaeCalculated()) {
			if (!template.singularitiesCalculated()) {
				template.extractSingularities(getDirBlockwise());
			}
			template.extractMinutiae(getSkeleton(), getMask(), 300);
		}

		Mat temp = new Mat();
		if (src.channels() == 1) Imgproc.cvtColor(src, temp, Imgproc.COLOR_GRAY2BGR);
		else src.copyTo(temp);
		template.drawMinutiae(temp);
		return temp;
	}

	public Template getTemplate() {
		return template;
	}

}
