package org.fpr.obj;

import org.opencv.core.Point;

public class Minutia {

	public Point pos;
	public double angle;
	public double radius;
	public double azimuth;
	public MinutiaType type;

	public Minutia(Point pos, double angle, double radius, double azimuth, MinutiaType type) {
		this.pos = pos;
		this.angle = angle;
		this.radius = radius;
		this.azimuth = azimuth;
		this.type = type;
	}

	public double angleBetween(Minutia m) {
		return Math.atan2(pos.y - m.pos.y, m.pos.x - pos.x);
	}

	public double distanceBetween(Minutia m) {
		return Math.sqrt(Math.pow(m.pos.x - pos.x, 2) + Math.pow(pos.y - m.pos.y, 2));
	}

	public double facingAngle(Minutia m) {
		double diffAngle = angleBetween(m);
		double result = 0;
		if (type == MinutiaType.RIDGE_ENDING && m.type == MinutiaType.RIDGE_ENDING) {
			result = Math.min(getAbsAngleDiff(angle, Math.PI + diffAngle), getAbsAngleDiff(m.angle, diffAngle));
		}

		if (type == MinutiaType.BIFURCATION) {
			result = Math.min(getAbsAngleDiff(angle, diffAngle), getAbsAngleDiff(m.angle, Math.PI + diffAngle));
		}

		return result;
	}

	public static double getAbsAngleDiff(double alpha, double beta) {
		double a = normalizeAngle(normalizeAngle(alpha) - normalizeAngle(beta));
		return Math.abs(a);
	}

	public static double normalizeAngle(double a) {
		return a + (a > Math.PI ? -2 * Math.PI : (a < -Math.PI) ? 2 * Math.PI : 0);
	}
}
