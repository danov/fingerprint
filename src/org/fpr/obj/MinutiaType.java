package org.fpr.obj;

public enum MinutiaType {
	RIDGE_ENDING, BIFURCATION, LOOP, DELTA, WHORL
}
